require 'spec_helper'

describe Event do

	let(:user) { FactoryGirl.create(:user) }
  before do
    @event = user.events.build(title: "Lorem ipsum", from_time: "27-05-2014", to_time: "28-05-2014")
  end

  subject { @event }

  it { should be_valid }
  it { should respond_to(:title) }
  it { should respond_to(:user_id) }
  it { should respond_to(:from_time) }
  it { should respond_to(:to_time) }

  describe "when title is not present" do
    before { @event.title = " " }
    it { should_not be_valid }
  end

  # describe "when To Time is not present" do
  #   before { @event.to_time = " " }
  #   it { should_not be_valid }
  # end

  describe "when event id is not present" do
    before { @event.user_id = nil }
    it { should_not be_valid }
  end
	
	describe "when from Time is greater than to Time" do
    before { @event.from_time = "29-05-2014" }
    it { should_not be_valid }
	end
end
