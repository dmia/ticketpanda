require 'spec_helper'

describe User do
  before do
    @user = FactoryGirl.create(:user)
  end

  subject { @user }

  it { should be_valid }

  describe "when name is not present" do
    before { @user.username = " " }
    it { should_not be_valid }
  end

  describe "when email is not present" do
    before { @user.email = " " }
    it { should_not be_valid }
  end

  it "all existing users should have email" do
    @user.email.should_not be_empty
  end

  it "all existing users should have username" do
    @user.username.should_not be_empty
  end

  it "every existing user email should have specific format" do
    @user.email.should =~ /^.+@.+$/
  end

  it "all existing users should have password" do
  	@user.encrypted_password.should_not be_empty
  end

  describe "when password doesn't match confirmation" do
    before { @user.password_confirmation = "mismatch" }
    it { should_not be_valid }
	end

  it "should have normal email addresses" do
    User.new(username: "testname", email: "notniceemail@no", password: "passwordpass", password_confirmation: "passwordpass").should_not be_valid
    User.new(username: "testname", email: "", password: "passwordpass", password_confirmation: "passwordpass").should_not be_valid
    User.new(username: "testname", email: "@no.gr", password: "passwordpass", password_confirmation: "passwordpass").should_not be_valid
    User.new(username: "testname", email: "__-notniceemail@no", password: "passwordpass", password_confirmation: "passwordpass").should_not be_valid
    User.new(username: "testname", email: "notniceemail@no.com", password: "passwordpass", password_confirmation: "passwordpass").should be_valid
  end

  it "should not let admins without password or with password less than 8 letters to exist" do
    User.new(username: "testname", email: "passemail@pass.gr", password: "", password_confirmation: "").should_not be_valid
    User.new(username: "testname", email: "passemail@pass.gr", password: "passwor", password_confirmation: "passwor").should_not be_valid
    User.new(username: "testname", email: "passemail@pass.gr", password: "password", password_confirmation: "password").should be_valid
  end

  describe "email address with mixed case" do
    let(:mixed_case_email) { "Foo@ExAMPle.CoM" }

    it "should be saved as all lower-case" do
      @user.email = mixed_case_email
      @user.save
      expect(@user.reload.email).to eq mixed_case_email.downcase
    end
  end

  describe "associations" do
    before do
      @user.save
      FactoryGirl.create(:event, user: @user)
    end
    it "should destroy associated events" do
      events = @user.events.to_a
      @user.destroy
      expect(events).not_to be_empty
      events.each do |event|
        expect(Event.where(id: event.id)).to be_empty
      end
    end
  end

end
