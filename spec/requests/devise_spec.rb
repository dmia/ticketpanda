require 'spec_helper'

describe "Devise functions" do
  	subject { page }

  	describe "signup page" do
    before { visit new_user_registration_path }

    it { should have_content('Sign up') }
    # it { should have_title(full_title('Sign up')) }
  end

 describe "signup" do

    before { visit new_user_registration_path }

    let(:submit) { "formSignUp" }

    describe "with invalid information" do
      it "should not create a user" do
        expect { click_button submit }.not_to change(User, :count)
      end
      describe "after submission" do
        before { click_button submit }

        it { should have_content('Sign up') }
        it { should have_selector('div.alert.alert-warning') }
      end
    end

    describe "with valid information" do
      before do
        fill_in "Username",         with: "abc"
        fill_in "Email",        with: "spectesting@example.com"
        fill_in "Password",     with: "foobar12"
        fill_in "Password confirmation", with: "foobar12"
      end

      it "should create a user" do
        expect { click_button submit }.to change(User, :count).by(1)
      end

      describe "after saving the user" do
        before { click_button submit }
        let(:user) { User.find_by(email: 'spectesting@example.com') }

        # it { should have_link('Sign out') }
        # it { should have_content(user.username) }
        it { should have_selector('div.alert.alert-notice', text: 'Welcome') }
      end
    end
  end


end