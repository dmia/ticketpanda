FactoryGirl.define do
  factory :user do
  	username "abcd"
    email "testemail2@example.com"
    password "foobar12"
    password_confirmation "foobar12"
  end

  factory :event do
  	title "Ëvent Title"
  	from_time "27-05-2014"
  	to_time "28-05-2014"
  	user
  end
end