# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

50.times { |i| Event.create(title: "Event Sample #{i}", from_time: 20.days.from_now+i.day, 
to_time: 20.days.from_now + (i+1).day, user_id:3 ) }
