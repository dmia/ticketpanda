class Event < ActiveRecord::Base
	belongs_to :user
	validates :title, presence: true
	validates :from_time, presence: true 
	validates :to_time, presence: true
	validates :user_id, presence: true, numericality: true
	validate :greater_than_from_time, :cannot_be_in_past => :from_time,
	 :cannot_be_in_past => :to_time


	def greater_than_from_time
		if from_time>to_time
			errors.add(:to_time, "can't be earlier than From Time")
		end
	end

	def cannot_be_in_past date
		if date.present? && date< Date.today
			errors.add(:date, "can't be in the past")
		end
	end
end
