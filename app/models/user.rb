class User < ActiveRecord::Base
  # Associations
  has_many :events, dependent: :destroy
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:login]

  #validations for username field since it is not covered by Devise.
  validates :username, presence: true, length: {maximum: 255}, uniqueness: { case_sensitive: false }, 
  			format: { with: /\A[a-zA-Z0-9]*\z/, message: "may only contain letters and numbers." }

  # Virtual attribute for authenticating by either username or email
  # This is in addition to a real persisted field like 'username'. More like a transient variable
   attr_accessor :login

  # lower(username) used so that username is the same as UserName (case insensitive)
   def self.find_first_by_auth_conditions(warden_conditions)
   	conditions = warden_conditions.dup
   	if login = conditions.delete(:login)
   		where(conditions).where(["lower(username) = :value OR lower(email) = lower(:value)", { :value => login }]).first
   	else
   	    where(conditions).first
   	    end
   	end

end
