json.array!(@events) do |event|
  json.extract! event, :id, :title, :from_time, :to_time, :user_id
  json.url event_url(event, format: :json)
end
